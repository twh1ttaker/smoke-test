package config

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
)

var (
	appConfig Configuration
)

type Configuration struct {
	gateKeeperHealthEndpoint       string // Gatekeper health endpoint
	gouiHealthEndpoint             string // GOUI endpoint which should return 200 if it is up. Requires grafana api token
	grafanaApiToken                string // API token created in grafana used to authenticate calls
	sentryDSN                      string // Sentry dsn used to write errors for error tracking
	groupErrorTrackingEndpoint     string // ET group endpoint for which we can read an error
	errorTrackingIngestionDelaySec int    // Delay between writting and reading an error to ensure that reading will succeed
}

func GetGateKeeperHealthEndpoint() string {
	return appConfig.gateKeeperHealthEndpoint
}

func GetGouiHealthEndpoint() string {
	return appConfig.gouiHealthEndpoint
}

func GetGrafanaAPItoken() string {
	return appConfig.grafanaApiToken
}

func GetSentryDSN() string {
	return appConfig.sentryDSN
}

func GetGroupErrorTrackingEndpoint() string {
	return appConfig.groupErrorTrackingEndpoint
}

func GetErrorTrackingIngestionDelaySec() int {
	return appConfig.errorTrackingIngestionDelaySec
}

func InitializeConfig() error {
	appConfig.gateKeeperHealthEndpoint = strings.TrimSpace(os.Getenv("GATEKEEPER_HEALTH_CHECK"))
	if appConfig.gateKeeperHealthEndpoint == "" {
		return errors.New("Gatekeeper health url cannot be empty. Ensure GATEKEEPER_HEALTH_CHECK is set properly")
	}

	appConfig.gouiHealthEndpoint = strings.TrimSpace(os.Getenv("GOUI_HEALTH_CHECK"))
	if appConfig.gouiHealthEndpoint == "" {
		return errors.New("GOUI health url cannot be empty. Ensure GOUI_HEALTH_CHECK is set properly")
	}

	appConfig.grafanaApiToken = strings.TrimSpace(os.Getenv("GRAFANA_API_TOKEN"))
	if appConfig.grafanaApiToken == "" {
		return errors.New("Grafana API token cannot be empty. Ensure GRAFANA_API_TOKEN is set properly")
	}

	appConfig.sentryDSN = strings.TrimSpace(os.Getenv("SENTRY_DSN"))
	if appConfig.sentryDSN == "" {
		return errors.New("Sentry DSN cannot be empty. Ensure SENTRY_DSN is set properly")
	}

	appConfig.groupErrorTrackingEndpoint = strings.TrimSpace(os.Getenv("GROUP_ERROR_TRACKING_ENDPOINT"))
	if appConfig.groupErrorTrackingEndpoint == "" {
		return errors.New("Group error tracking endpoint cannot be empty. Ensure GROUP_ERROR_TRACKING_ENDPOINT is set properly")
	}

	delaystr := strings.TrimSpace(os.Getenv("ERROR_TRACKING_INGESTION_DELAY_SEC"))
	if delaystr == "" {
		return errors.New("ERROR_TRACKING_INGESTION_DELAY_SEC cannot be empty")
	}
	var err error
	appConfig.errorTrackingIngestionDelaySec, err = strconv.Atoi(delaystr)
	if err != nil {
		return errors.New("ERROR_TRACKING_INGESTION_DELAY_SEC needs to be an integer number")
	}
	log.Printf("Config: Error Tracking ingestion delay in sec = %v", appConfig.errorTrackingIngestionDelaySec)

	return nil
}
