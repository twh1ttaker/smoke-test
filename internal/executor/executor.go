package executor

import (
	"log"

	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/smoketests/errortracking"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/smoketests/gatekeeper"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/smoketests/goui"
)

type TestExecutor interface {
	RunSmokeTest() error
}

type ComponentUptimeTest interface {
	RunUptimeTest() error
}

type PlatformExecutor struct{}

func (g *PlatformExecutor) RunSmokeTest() error {
	var err error

	// Run test on GK
	gk := gatekeeper.GatekeeperTestRunner{
		Client: &http_client.HttpRequestWrapper{},
	}
	err = gk.RunUptimeTest()
	if err != nil {
		return err
	}
	log.Println("---> Gatekeeper smoke test passed <---")

	// Run test on GOUI
	goui := goui.GouiTestRunner{
		Client: &http_client.HttpRequestWrapper{},
	}
	err = goui.RunUptimeTest()
	if err != nil {
		return err
	}
	log.Println("---> GOUI smoke test passed <---")

	// Run test on error-tracking write path
	et := errortracking.ErrorTrackingTestRunner{
		SentryClient: &errortracking.SentryClientWrapper{},
		HttpClient:   &http_client.HttpRequestWrapper{},
	}
	err = et.RunUptimeTest()
	if err != nil {
		return err
	}
	log.Println("---> Error-Tracking read and write path smoke tests passed <---")
	return nil
}
