package config

import (
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	gateKeeperHealthEndpoint       = "gatekeeper_endpoint"
	gouiHealthEndpoint             = "goui_endpoint"
	grafanaApiToken                = "token"
	sentryDSN                      = "sentry_dsn"
	groupErrorTrackingEndpoint     = "error_tracking_endpoint"
	errorTrackingIngestionDelaySec = "2"
)

func unsetAllEnvVars() {
	os.Unsetenv("GATEKEEPER_HEALTH_CHECK")
	os.Unsetenv("GOUI_HEALTH_CHECK")
	os.Unsetenv("GRAFANA_API_TOKEN")
	os.Unsetenv("SENTRY_DSN")
	os.Unsetenv("GROUP_ERROR_TRACKING_ENDPOINT")
	os.Unsetenv("ERROR_TRACKING_INGESTION_DELAY_SEC")
}

func setAllEnvVars() {
	os.Setenv("GATEKEEPER_HEALTH_CHECK", gateKeeperHealthEndpoint)
	os.Setenv("GOUI_HEALTH_CHECK", gouiHealthEndpoint)
	os.Setenv("GRAFANA_API_TOKEN", grafanaApiToken)
	os.Setenv("SENTRY_DSN", sentryDSN)
	os.Setenv("GROUP_ERROR_TRACKING_ENDPOINT", groupErrorTrackingEndpoint)
	os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", errorTrackingIngestionDelaySec)
}

func TestGetters(t *testing.T) {
	assert := assert.New(t)
	setAllEnvVars()
	assert.Nil(InitializeConfig())

	t.Run("GetGateKeeperHealthEndpoint should return gateKeeperHealthEndpoint", func(t *testing.T) {
		assert.Equal(gateKeeperHealthEndpoint, GetGateKeeperHealthEndpoint())
	})

	t.Run("GetGouiHealthEndpoint should return gouiHealthEndpoint", func(t *testing.T) {
		assert.Equal(gouiHealthEndpoint, GetGouiHealthEndpoint())
	})

	t.Run("GetGrafanaAPItoken should return grafanaApiToken", func(t *testing.T) {
		assert.Equal(grafanaApiToken, GetGrafanaAPItoken())
	})

	t.Run("GetSentryDSN should return sentryDSN", func(t *testing.T) {
		assert.Equal(sentryDSN, GetSentryDSN())
	})

	t.Run("GetGroupErrorTrackingEndpoint should return groupErrorTrackingEndpoint", func(t *testing.T) {
		assert.Equal(groupErrorTrackingEndpoint, GetGroupErrorTrackingEndpoint())
	})

	t.Run("GetErrorTrackingIngestionDelaySec should return errorTrackingIngestionDelaySec", func(t *testing.T) {
		n, _ := strconv.Atoi(errorTrackingIngestionDelaySec)
		assert.Equal(n, GetErrorTrackingIngestionDelaySec())
	})
}

func TestInitializeConfig(t *testing.T) {
	assert := assert.New(t)
	unsetAllEnvVars()

	t.Run("Should fail if GATEKEEPER_HEALTH_CHECK is not set", func(t *testing.T) {
		assert.True(strings.Contains(InitializeConfig().Error(), "GATEKEEPER_HEALTH_CHECK"))
	})

	t.Run("Should fail if GOUI_HEALTH_CHECK is not set", func(t *testing.T) {
		os.Setenv("GATEKEEPER_HEALTH_CHECK", gateKeeperHealthEndpoint)
		assert.True(strings.Contains(InitializeConfig().Error(), "GOUI_HEALTH_CHECK"))
	})

	t.Run("Should fail if GRAFANA_API_TOKEN is not set", func(t *testing.T) {
		os.Setenv("GOUI_HEALTH_CHECK", gouiHealthEndpoint)
		assert.True(strings.Contains(InitializeConfig().Error(), "GRAFANA_API_TOKEN"))
	})

	t.Run("Should fail if SENTRY_DSN is not set", func(t *testing.T) {
		os.Setenv("GRAFANA_API_TOKEN", grafanaApiToken)
		assert.True(strings.Contains(InitializeConfig().Error(), "SENTRY_DSN"))
	})

	t.Run("Should fail if GROUP_ERROR_TRACKING_ENDPOINT is not set", func(t *testing.T) {
		os.Setenv("SENTRY_DSN", sentryDSN)
		assert.True(strings.Contains(InitializeConfig().Error(), "GROUP_ERROR_TRACKING_ENDPOINT"))
	})

	t.Run("Should fail if ERROR_TRACKING_INGESTION_DELAY_SEC is not set", func(t *testing.T) {
		os.Setenv("GROUP_ERROR_TRACKING_ENDPOINT", groupErrorTrackingEndpoint)
		assert.True(strings.Contains(InitializeConfig().Error(), "ERROR_TRACKING_INGESTION_DELAY_SEC"))
	})

	t.Run("Should fail if ERROR_TRACKING_INGESTION_DELAY_SEC is set but is not a number", func(t *testing.T) {
		os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", "this value should be a number")
		assert.True(strings.Contains(InitializeConfig().Error(), "ERROR_TRACKING_INGESTION_DELAY_SEC needs to be an integer number"))
	})

	t.Run("Should succeed if all env vars are set", func(t *testing.T) {
		os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", errorTrackingIngestionDelaySec)
		assert.Nil(InitializeConfig())
	})
}
