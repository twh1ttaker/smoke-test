package goui

import (
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/config"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
)

type GouiTestRunner struct {
	Client http_client.HttpRequestInterface
}

func (g *GouiTestRunner) RunUptimeTest() error {
	url := config.GetGouiHealthEndpoint()

	req, err := g.Client.CreateRequest(http.MethodGet, config.GetGouiHealthEndpoint(), nil)
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.GetGrafanaAPItoken()))
	resp, err := g.Client.Do(req)
	defer func() { _ = resp.Body.Close() }()
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("goui: url: %v, status code: %v, message: %v", url, resp.StatusCode, resp.Status)
	}

	return nil
}
