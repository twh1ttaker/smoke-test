.SHELLFLAGS = -euc
SHELL=/bin/bash

# The following are env vars needed by smoke-test application
export  GATEKEEPER_HEALTH_CHECK=https://observe.staging.gitlab.com/readyz
export  GOUI_HEALTH_CHECK=https://observe.gitlab.com/9970/api/dashboards/home
export  GROUP_ERROR_TRACKING_ENDPOINT=https://observe.gitlab.com/v2/errortracking/14485840/projects/32149347/errors
# ERROR_TRACKING_INGESTION_DELAY_SEC represents the delay that we should have between sending and reading the same error. We choose a value that
# will give us confidence that the error should already be ingested.
export  ERROR_TRACKING_INGESTION_DELAY_SEC=6
# use buildkit for better multi-platform variable support.
# see https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope
export DOCKER_BUILDKIT=1

# Registry address for pushing images to
export DOCKER_IMAGE_REGISTRY ?= registry.gitlab.com/gitlab-org/opstrace/smoke-test

ifdef CI_COMMIT_TAG
  # We are doing a release!
  export DOCKER_IMAGE_TAG := ${CI_COMMIT_TAG}
else
  # Ordinary build
  # We need to use here only the most basic tools, as CI uses lots of different
  # containers, that provide different versions of tool, if at all.
  DOCKER_IMAGE_TAG := $(shell git rev-parse --short=8 HEAD | tr -d '\n')
  ifneq ($(shell git status --porcelain),)
	DOCKER_IMAGE_TAG := $(DOCKER_IMAGE_TAG).dirty
  endif
endif

# DOCKER_IMAGE_NAME is lazily evaluated while traversing the per-dir Makefiles
export DOCKER_IMAGE = ${DOCKER_IMAGE_REGISTRY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}

export GO_BUILD_LDFLAGS = \
	-X main.release=${DOCKER_IMAGE_TAG} \

docker-build: DOCKER_IMAGE_NAME = app
docker-build:
	docker build \
		--build-arg GO_BUILD_LDFLAGS \
		-f Dockerfile \
		-t ${DOCKER_IMAGE} \
			.
.PHONY: docker-push
docker-push: DOCKER_IMAGE_NAME = app
docker-push:
	docker push ${DOCKER_IMAGE}

.PHONY: docker-pull
docker-pull: DOCKER_IMAGE_NAME = app
docker-pull:
	docker pull ${DOCKER_IMAGE}

.PHONY: docker-run
docker-run: DOCKER_IMAGE_NAME = app
docker-run:
	docker run  -e GATEKEEPER_HEALTH_CHECK=${GATEKEEPER_HEALTH_CHECK} \
				-e GOUI_HEALTH_CHECK=${GOUI_HEALTH_CHECK} \
				-e GRAFANA_API_TOKEN=${GRAFANA_API_TOKEN} \
				-e SENTRY_DSN=${SENTRY_DSN} \
				-e GROUP_ERROR_TRACKING_ENDPOINT=${GROUP_ERROR_TRACKING_ENDPOINT} \
				-e ERROR_TRACKING_INGESTION_DELAY_SEC=${ERROR_TRACKING_INGESTION_DELAY_SEC} \
			    ${DOCKER_IMAGE}

.PHONY: run-tests
run-tests:
	go test -race ./... -v