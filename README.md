# Smoke Test

## Motivation
We need a mechanism to ensure [observe.gitlab.com](observe.gitlab.com) is up and in good health. The goal is to perform a quick smoke test (black box testing) to ensure that the crucial components of Gitlab Observability Platform on production works correctly.

## Implemented smoke tests
The components that are being tested are the following:
- **Gatekeeper:** is responsible for a big part of the authentication for Gitlab Observability Platform. We ensure that this component is working by calling the /readyz endpoint which is a health check endpoint.
- **GOUI:** we can smoke test this part by trying to access the GOUI on the root namespace. Authentication is performed using an api token created in grafana
- **Error Tracking:** for this component we write an error using the Sentry SDK and then we read it. This is enough to ensure that both read and write paths for error tracking are working correctly.

## Future smoke tests
We plan to implement a smoke test for traces once the feature is on production

## Setup
In order to run this application, first you need to generate a grafana API token. Visit the opstrace [GOUI](https://observe.gitlab.com/9970/group/apikeys), press add API key, give a name to the key and press add. Save the generated api key somewhere safe. Once you have the API key then you can execute the following commands:
```
export GRAFANA_API_TOKEN=<your_api_token>
make docker-build
make docker-run
```

If you have uncommited changes the makefile will atempt to run a .dirty image. To avoid that behavior try to stash your changes or set CI_COMMIT_TAG to the last commit short hash:
```
git rev-parse --short HEAD
```

You can run the unit tests of this app by running:
```
make run-tests
```

## CI pipeline
This project has a CI pipeline that gets executed
- if there is a change in the code
- based on a schedule every 5 minutes