package errortracking

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/config"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
)

const (
	SMOKE_TEST_ERROR_TEXT = "smoke-test-gop fake-error"
)

// Variable to be filled in by the makefile.
// Release is used when sending error events using the Sentry SDK.
// It refers to this application release.
var release string

// TransportWithHooks is an http.RoundTripper that wraps an existing
// http.RoundTripper adding hooks that run before and after each round trip.
type TransportWithHooks struct {
	http.RoundTripper
	Before func(*http.Request) error
	After  func(*http.Request, *http.Response, error) (*http.Response, error)
}

func (t *TransportWithHooks) RoundTrip(req *http.Request) (*http.Response, error) {
	if err := t.Before(req); err != nil {
		return nil, err
	}
	resp, err := t.RoundTripper.RoundTrip(req)
	return t.After(req, resp, err)
}

type ErrorTrackingTestRunner struct {
	SentryClient
	HttpClient http_client.HttpRequestInterface
	failure    error
}

func (g *ErrorTrackingTestRunner) RunUptimeTest() error {
	// We need to send errors to sentry in a sync way in order
	// to set an error before exiting the function in case something goes wrong
	sentrySyncTransport := sentry.NewHTTPSyncTransport()
	// We chose a number for timeout that give us enough confident that smoke-test
	// will not fail unexpectedly
	sentrySyncTransport.Timeout = time.Second * 6

	err := g.SentryClient.Init(sentry.ClientOptions{
		Debug:     true,
		Release:   release,
		Dsn:       config.GetSentryDSN(),
		Transport: sentrySyncTransport,
		HTTPTransport: &TransportWithHooks{
			RoundTripper: http.DefaultTransport,
			Before: func(req *http.Request) error {
				return nil
			},
			After: func(req *http.Request, resp *http.Response, err error) (*http.Response, error) {
				if err != nil {
					g.failure = err
					return nil, err
				}
				if resp != nil {
					log.Println("Sending data to Sentry:", resp.Status)
					if resp.StatusCode != http.StatusOK {
						g.failure = fmt.Errorf("sending error request to Gitlab Observability Platform failed with %v", resp.Status)
						return nil, g.failure
					}
				} else {
					g.failure = errors.New("sending error request to Gitlab Observability Platform failed due to nil response")
					return nil, g.failure
				}
				return resp, err
			},
		},
	})
	if err != nil {
		return fmt.Errorf("Failed to initialize Sentry: %v", err)
	}

	// First we smoke test the write path
	fakeError := errors.New("smoke-test-gop fake-error")
	eventID := g.SentryClient.CaptureException(fakeError)
	if g.failure != nil {
		log.Printf("Failed to send error: %v", err)
		return g.failure
	}
	log.Printf("reported to Sentry: %s - %v", fakeError, *eventID)

	// Then we smoke test the read path
	time.Sleep(time.Duration(config.GetErrorTrackingIngestionDelaySec()) * time.Second)
	url := config.GetGroupErrorTrackingEndpoint()
	req, err := g.HttpClient.CreateRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	// Query the last seen errors
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.GetGrafanaAPItoken()))
	resp, err := g.HttpClient.Do(req)
	defer func() { _ = resp.Body.Close() }()
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", url, resp.StatusCode, resp.Status)
	}

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var errorEvents []ErrorEvent
	err = json.Unmarshal(r, &errorEvents)
	if err != nil {
		return err
	}

	for _, e := range errorEvents {
		// Find the error we previously wrote
		if strings.Contains(e.Description, SMOKE_TEST_ERROR_TEXT) {
			// Resolve it to avoid Gitlab error pollution
			log.Println("Found " + SMOKE_TEST_ERROR_TEXT)
			resolveUrl := fmt.Sprintf("%s/%d", url, e.Fingerprint)

			body := UpdateErrorEvent{
				Status: "resolved",
			}
			postBody, err := json.Marshal(body)
			if err != nil {
				return err
			}
			req, err := g.HttpClient.CreateRequest(http.MethodPut, resolveUrl, bytes.NewReader(postBody))
			if err != nil {
				return err
			}

			req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.GetGrafanaAPItoken()))
			req.Header.Add("Content-Type", "application/json")
			resp, err := g.HttpClient.Do(req)
			defer func() { _ = resp.Body.Close() }()
			if err != nil {
				return err
			}

			if resp.StatusCode != http.StatusOK {
				return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", resolveUrl, resp.StatusCode, resp.Status)
			}
			log.Println("Resolved " + SMOKE_TEST_ERROR_TEXT)

			return nil
		}
	}

	return errors.New("failed to read " + SMOKE_TEST_ERROR_TEXT)

}

type ErrorEvent struct {
	Status      string `json:"status,omitempty"`
	ProjectId   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	Eventcount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

type UpdateErrorEvent struct {
	Status string `json:"status"`
}
