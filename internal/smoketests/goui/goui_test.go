package goui

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
	commons "gitlab.com/gitlab-org/opstrace/smoke-test/internal/smoketests"
)

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

func TestGOUIRunUptimeTest(t *testing.T) {
	assert := assert.New(t)
	httpMock := &http_client.MockHttpClient{}
	goui := GouiTestRunner{
		Client: httpMock,
	}
	commons.InitializeConfig()

	t.Run("Fails if CreateRequest returns an error", func(t *testing.T) {
		expectedErr := errors.New("mock-error")
		httpMock.On("CreateRequest", http.MethodGet, commons.GouiHealthEndpoint, nil).Times(1).Return(&http.Request{}, expectedErr)
		assert.Equal(expectedErr, goui.RunUptimeTest())
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if Do returns an error", func(t *testing.T) {
		expectedErr := errors.New("mock-error")
		expectedRequest := http.Request{Method: http.MethodGet, Header: make(map[string][]string, 1)}
		emptyResponse := &http.Response{Body: io.NopCloser(strings.NewReader(""))}
		httpMock.On("CreateRequest", http.MethodGet, commons.GouiHealthEndpoint, nil).Times(1).Return(&expectedRequest, nil)
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(emptyResponse, expectedErr)
		assert.Equal(expectedErr, goui.RunUptimeTest())
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if status code is not 200", func(t *testing.T) {
		expectedRequest := http.Request{Method: http.MethodGet, Header: make(map[string][]string, 1)}
		httpMock.On("CreateRequest", http.MethodGet, commons.GouiHealthEndpoint, nil).Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 404, Body: nopCloser{bytes.NewBufferString("data")}}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		assert.True(strings.Contains(goui.RunUptimeTest().Error(), "status code: 404"))
		httpMock.AssertExpectations(t)
	})

	t.Run("Succeeds", func(t *testing.T) {
		expectedRequest := http.Request{Method: http.MethodGet, Header: make(map[string][]string, 1)}
		httpMock.On("CreateRequest", http.MethodGet, commons.GouiHealthEndpoint, nil).Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 200, Body: nopCloser{bytes.NewBufferString("data")}}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		assert.Nil(goui.RunUptimeTest())
		httpMock.AssertExpectations(t)
	})
}
